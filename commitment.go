package main

import (
	"crypto/sha256"
	"fmt"
)

func commit(hash []byte) [32]byte {
	var buf [64]byte
	var sli []byte
	sli = buf[0:0]

	var whitepaper = hex2byte32([]byte("6AFBAC595C1D07A3D4C5179758F5BCE4462A6C263F6E6DFCD942011433ADAAE7"))

	sli = append(sli, whitepaper[0:]...)
	sli = append(sli, hash[0:]...)

	return nethash(sli)
}

func merkle(a []byte, b []byte) [32]byte {
	var buf [64]byte
	var sli []byte
	sli = buf[0:0]

	sli = append(sli, a[0:]...)
	sli = append(sli, b[0:]...)

	return nethash(sli)
}

func Testnet() bool {
	return u_config.testnet
}

func nethash(sli []byte) [32]byte {
	if Testnet() {
		var whitepaper = hex2byte32([]byte("2e3841b6e75e9717ab7d2a8b57248b7f611a5473381b5e432aaf8fe88874fbfe"))

		sli = append(whitepaper[:], sli...)
		sli = append(whitepaper[:], sli...)
	}
	return sha256.Sum256(sli)
}

// comb address is uppercase on main-net, and lower-case on testnet
func CombAddr(x [32]byte) string {
	if Testnet() {
		return fmt.Sprintf("%x", x)
	}
	return fmt.Sprintf("%X", x)
}