package main

import (
	//"fmt"
	"bufio"
	"io"
	"log"
	"os"
	"strings"
)

var u_config UserConfig

// Holds information related to general configuration.
type UserConfig struct {
	username    string
	password    string
	regtest     bool
	testnet     bool
	listen_port string
	reorg_type  string
	logfile     string
}

func load_config() {

	// Setup defaults
	u_config.reorg_type = "miner"
	u_config.regtest = false
	u_config.listen_port = "2121"

	// Check for config
	if _, err := os.Stat("config.txt"); err != nil {
		if os.IsNotExist(err) {
			return
		}
	}

	log.Println("Retrieving config...")

	f, conferr := os.Open("config.txt")
	if conferr != nil {
		log.Fatal("config open error: ", conferr)
	}

	rd := bufio.NewReader(f)

	finished := false

	for {
		line, rerr := rd.ReadString('\n')
		if rerr != nil {
			if rerr == io.EOF {
				finished = true
			} else {
				log.Fatal("config read error: ", rerr)
			}
		}

		var tag string
		var info string

		// Read the line until "="
		i := -1
		for ii := range line {
			if string(line[ii]) == "=" {
				i = ii
				break
			}
		}

		switch i <= 0 {
		case true:

		default:
			tag = string(line[:i+1])
			info = strings.TrimRight(string(line[i+1:]), "\r\n")
			log.Println(tag, info)
		}

		switch tag {
		case "btcuser=":
			u_config.username = info
		case "btcpass=":
			u_config.password = info
		case "btcmode=":
			switch info {
			case "regtest":
				u_config.regtest = true
			case "testnet":
				u_config.testnet = true
			}
		case "port=":
			u_config.listen_port = info
		case "reorg_type=":
			switch info {
			case "direct":
				u_config.reorg_type = "direct"
			default:
				u_config.reorg_type = "miner"
			}
		case "logfile=":
			u_config.logfile = info
		default:

		}

		if finished {
			break
		}
	}
	f.Close()
}