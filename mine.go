package main

import (
	"crypto/sha256"
	"fmt"
	"hash"
	"log"
	"net/http"
	"sync"
)

// This file oversees the portion of mining process that is processing incoming commitments before they are stored to the DB or Memory

var commits_mutex sync.RWMutex

var commits map[[32]byte]utxotag
var combbases map[[32]byte]struct{}

var commit_cache_mutex sync.Mutex

var commit_currently_loaded utxotag
var commit_cache [][32]byte
var commit_tag_cache []utxotag
var commit_rollback [][32]byte
var commit_rollback_tags []utxotag

func init() {
	commits = make(map[[32]byte]utxotag)
	combbases = make(map[[32]byte]struct{})
}

func miner_unmine_pending_commits() {
	commit_cache_mutex.Lock()
	commit_cache = nil
	commit_tag_cache = nil
	commit_rollback = nil
	commit_rollback_tags = nil
	commit_cache_mutex.Unlock()
}

func height_view(w http.ResponseWriter, r *http.Request) {
	// Used to tell BTC what height to start at. Done after 6 flush validation
	// Replacing this with a 99999999 response to make this code compatible with the modded BTC
	fmt.Fprintf(w, "99999999")

}

func miner_mine_commit_pulled(commitinput string, utxotagval utxotag, direction int) hash.Hash {
	// This gets run when new blocks come in.

	// this is not address related, so we check hex is upper on all networks

	err1 := checkHEX32upper(commitinput)
	if err1 != nil {
		log.Println("error mining by using commit: ", err1)
		return nil
	}

	return miner_mine_commit_internal(DummyHttpWriter{}, commitinput, utxotagval, direction)
}

var mine_cycles = 0

func miner_mine_commit_internal(w http.ResponseWriter, commitinput string, tag utxotag, direction_mine_unmine int) (sum hash.Hash) {

	// This is run to input commits into program memory, either from the DB or when new blocks come in.
	var unwritten bool
	var reorg_height uint64
	var rawcommit = hex2byte32([]byte(commitinput))

	// Finish mining the block, if in process
	if direction_mine_unmine == UTAG_END_OF_BLOCK {

		commit_cache_mutex.Lock()
		commits_mutex.Lock()

		posttag(&commit_currently_loaded, uint64(tag.height))
		if len(commit_rollback) > 0 && len(commit_cache) > 0 {
			log.Println("bidirectional block not supported")
			goto adios
		} else if len(commit_rollback) > 0 {

			// UNMINE STEP 2
			for i := range commit_rollback {
				if tagcommit, ok5 := commits[commit_rollback[i]]; ok5 {

					var basetag = commit_rollback_tags[i]
					var ctag = tagcommit
					var btag = basetag

					if utag_cmp(&ctag, &btag) != 0 {
						continue
					}

					var bheight = uint64(btag.height)

					if _, ok6 := combbases[commit_rollback[i]]; ok6 {

						segments_coinbase_unmine(commit_rollback[i], bheight)
						delete(combbases, commit_rollback[i])

					}

					break
				}
			}

		} else if len(commit_cache) > 0 {

			for i := range commit_cache {

				if _, ok5 := commits[commit_cache[i]]; !ok5 {
					var basetag = commit_tag_cache[i]
					var btag = basetag

					var bheight = uint64(btag.height)

					segments_coinbase_mine(commit_cache[i], bheight)
					combbases[commit_cache[i]] = struct{}{}

					break
				}
			}

		} else {
			goto adios
		}

		// Applies unmine to commits.db
		for i := len(commit_rollback) - 1; i >= 0; i-- {
			key := commit_rollback[i]

			if tagcommit, ok5 := commits[key]; !ok5 {
			} else {

				taggy := commit_rollback_tags[i]

				var ctag = tagcommit
				var btag = taggy

				if utag_cmp(&ctag, &btag) == 0 {
					//CommitDbUnWrite(key)
					db_entry := utxotag_to_leveldb(tagcommit) // Mod to a db entry
					CommitLvlDbUnWrite(key, db_entry)
					delete(commits, key)
					unwritten = true

					if enable_used_key_feature {

						log.Println("reorg commit height", ctag.height)

						reorg_height = uint64(ctag.height)

						used_key_commit_reorg(key, reorg_height)
					}
				}
			}
		}

		for _, key := range commit_rollback {
			// Applies UNMINE changes to the active wallet data?
			if _, ok5 := commits[key]; ok5 {
				continue
			}

			merkle_unmine(key)
			tx_leg_rollback(key)
		}

		commit_rollback = nil
		commit_rollback_tags = nil
		sum = sha256.New()

		for key, val := range commit_cache {
			// This is the prexisting commit check.
			if _, ok5 := commits[val]; ok5 {
			} else {

				if commitsdb == nil {
					log.Fatal("Commits DB not open for mining?")
				}
				//CommitDbWrite(val, hex2byte8(serializeutxotag(commit_tag_cache[key])))
				if initial_writeback_over {

					var levelkey = utxotag_to_leveldb(commit_tag_cache[key])

					sum.Write(levelkey)
					sum.Write(val[:])

					// Write
					CommitLvlDbWrite(val, levelkey)

				}

				commits[val] = commit_tag_cache[key]
				mine_cycles++
			}
		}

		for iter, key := range commit_cache {
			var tagval = commit_tag_cache[iter]

			merkle_mine(key)
			tx_mine(key, tagval)
		}

		resetgraph()

		if unwritten && enable_used_key_feature {
			log.Println("reorg block height", reorg_height)
			used_key_height_reorg(reorg_height)
		}

		commit_cache = nil
		commit_tag_cache = nil
	adios:

		commits_mutex.Unlock()
		commit_cache_mutex.Unlock()

		return sum
	}

	var is_coinbase bool

	commit_cache_mutex.Lock()

	is_coinbase = len(commit_cache)+len(commit_rollback) == 0

	if is_coinbase && direction_mine_unmine == UTAG_MINE && commit_currently_loaded.height >= tag.height {
		commit_cache_mutex.Unlock()

		if w != nil {
			fmt.Fprintf(w, "error: mined first commitment must be on greater height\n")
		}
		fmt.Printf("error: mined first commitment must be on greater height %d %d\n", commit_currently_loaded.height, tag.height)
		return nil
	}
	if is_coinbase && direction_mine_unmine == UTAG_UNMINE && commit_currently_loaded.height < tag.height {
		commit_cache_mutex.Unlock()

		if w != nil {
			fmt.Fprintf(w, "error: unmined first commitment must be on smaller height\n")
		}
		fmt.Printf("error: unmined first commitment must be on smaller height\n")
		return nil
	}
	if !is_coinbase && commit_currently_loaded.height != tag.height {
		commit_cache_mutex.Unlock()

		if w != nil {
			fmt.Fprintf(w, "error: commitment must be on same height as first commitment\n")
		}
		fmt.Printf("error: commitment must be on same height as first commitment\n")
		return nil
	}

	// UNMINE STEP 1
	if direction_mine_unmine == UTAG_UNMINE {

		commit_rollback = append(commit_rollback, rawcommit)
		commit_rollback_tags = append(commit_rollback_tags, tag)

	} else if direction_mine_unmine == UTAG_MINE {
		commit_cache = append(commit_cache, rawcommit)
		//mine_cycles++
		commit_tag_cache = append(commit_tag_cache, tag)
	}

	commits_mutex.Lock()
	commit_currently_loaded = tag
	commits_mutex.Unlock()
	commit_cache_mutex.Unlock()

	return nil
}

func tx_leg_rollback(key [32]byte) {
	txleg_mutex.RLock()

	txlegs_each_leg_target(key, func(tx *[32]byte) bool {

		segments_transaction_mutex.Lock()
		var txdata = segments_transaction_data[*tx]
		var actuallyfrom = txdata[21]

		var ok = false

		for i := uint(0); i < 21; i++ {
			if commit(txdata[i][0:]) == key {

				ok = true
				break
			}
		}
		if ok {
			segments_transaction_untrickle(nil, actuallyfrom, 0xffffffffffffffff)

			delete(segments_transaction_next, actuallyfrom)

		}

		segments_transaction_mutex.Unlock()

		return true
	})

	txleg_mutex.RUnlock()
}