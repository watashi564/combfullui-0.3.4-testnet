package main

import (
	"crypto/aes"
	"crypto/sha256"
	"encoding/binary"
	"fmt"
	"github.com/syndtr/goleveldb/leveldb"
	"github.com/syndtr/goleveldb/leveldb/opt"
	"github.com/syndtr/goleveldb/leveldb/util"
	"hash"
	"log"
	"sync"
)

type Commit_Block_Metadata struct {
	// Put header info here later
	Height      uint64 // the block height
	Hash        string // the block hash
	Fingerprint string // the finger print of all the commits CAT together and run through SHA256
}

const COMMITS_LVLDB_PATH = "commits"
const COMMITS_LVLDB_PATH_REGTEST = "commits_regtest"
const COMMITS_LVLDB_PATH_TESTNET = "commits_testnet"
const FLUSH1 = "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"

var commitsdb_mutex sync.Mutex
var commitsdb *leveldb.DB
var commitsbatch *leveldb.Batch

func CommitLvlDbNewBatch() {
	commitsbatch = new(leveldb.Batch)
}

func CommitLvlDbWriteBatch() {
	err := commitsdb.Write(commitsbatch, &opt.WriteOptions{
		Sync: true,
	})
	if err != nil {
		log.Fatal("DB write batch error: ", err)
	}
	commitsbatch = nil
}

func CommitLvlDbOpen() {
	path := COMMITS_LVLDB_PATH
	if u_config.regtest {
		path = COMMITS_LVLDB_PATH_REGTEST
	}
	if u_config.testnet {
		path = COMMITS_LVLDB_PATH_TESTNET
	}
	db, err := leveldb.OpenFile(path, &opt.Options{
		Compression: opt.NoCompression,
	})
	if err != nil {
		log.Fatal("DB open error: ", err)
	}
	commitsdb_mutex.Lock()
	commitsdb = db
}

func CommitLvlDbWrite(address [32]byte, tag []byte) {
	if !initial_writeback_over {
		return
	}
	if commitsdb == nil {
		return
	}

	// Convert to general bytes([:]) and store
	commitsbatch.Put(tag[:], address[:])

	// begin aes

	var aestmp [32]byte

	var aes, err5 = aes.NewCipher(address[0:])
	if err5 != nil {
		log.Fatal(err5)
	}

	aes.Encrypt(aestmp[0:16], database_aes[0:16])
	aes.Decrypt(aestmp[16:32], database_aes[16:32])

	for i := 8; i < 16; i++ {
		aestmp[i], aestmp[8+i] = aestmp[8+i], aestmp[i]
	}

	aes.Encrypt(database_aes[0:16], aestmp[0:16])
	aes.Decrypt(database_aes[16:32], aestmp[16:32])

	// end aes
}

func CommitLvlDbUnCompute(address [32]byte) {
	// begin aes

	var aestmp [32]byte

	var aes, err5 = aes.NewCipher(address[0:])
	if err5 != nil {
		log.Println("ERROR: CANNOT USE CIPHER")
		return
	}

	aes.Decrypt(aestmp[0:16], database_aes[0:16])
	aes.Encrypt(aestmp[16:32], database_aes[16:32])

	for i := 8; i < 16; i++ {
		aestmp[i], aestmp[8+i] = aestmp[8+i], aestmp[i]
	}

	aes.Decrypt(database_aes[0:16], aestmp[0:16])
	aes.Encrypt(database_aes[16:32], aestmp[16:32])

	// end aes
}
func CommitLvlDbUnWrite(address [32]byte, tag []byte) {
	// debug
	//res, _ := commitsdb.Get(tag, nil)
	//fmt.Println("address", fmt.Sprintf("%x", res))

	commitsbatch.Delete(tag)

	CommitLvlDbUnCompute(address)

}

func CommitLvlDbClose() {
	if commitsdb == nil {
		return
	}
	commitsdb.Close()
	commitsdb = nil
	commitsdb_mutex.Unlock()
}

func deserialize_metadata(height int, inc_val []byte) *Commit_Block_Metadata {

	//Hash([:64]) CAT Fingerprint[64:]
	val := string(inc_val)

	return &Commit_Block_Metadata{
		Height:      uint64(height),
		Hash:        val[0:64],
		Fingerprint: val[64:128],
	}

}

type commitLvlOngoingLoad struct {
	deletor bool
	meta    *Commit_Block_Metadata
}

func (l *commitLvlOngoingLoad) commitLvlHandleBlock(tag_height uint64, val []byte, sum hash.Hash) {
	var chk [32]byte

	if l.meta != nil {

		sum.Write([]byte(l.meta.Hash))
		sum.Sum(chk[0:0:32])

		if fmt.Sprintf("%x", chk) != l.meta.Fingerprint {
			l.deletor = true

			CommitLvlDbNewBatch()

		}

		//log.Printf("%d, previous sum:%x, next_sum: %s\n", l.meta.Height, chk, l.meta.Fingerprint)

		sum.Reset()

		if !l.deletor {

			// Mine the flush, important, for previous block
			miner_mine_commit_internal(&DummyHttpWriter{}, FLUSH1, new_flush_utxotag(uint64(l.meta.Height)), UTAG_END_OF_BLOCK)
			put_block(uint64(l.meta.Height), l.meta)

		} else {
			miner_unmine_pending_commits()

			log.Println("need deletion:", l.meta.Height)

			iter := commitsdb.NewIterator(&util.Range{
				Start: new_height_tag(uint64(l.meta.Height)),
				Limit: new_height_tag(uint64(l.meta.Height + 1))}, nil)
			for iter.Next() {
				if len(iter.Key()) == 8 || len(iter.Key()) == 16 {
					err := commitsdb.Delete(iter.Key(), nil)
					if err != nil {
						log.Fatal("Hash Erase error: ", err)
					}
				}
			}
			iter.Release()
			err := iter.Error()
			if err != nil {
				log.Fatal("Hash Erase error: ", err)
			}

		}
	}

	if val != nil {
		l.meta = deserialize_metadata(int(tag_height), val)

	}
}

func CommitLvlDbLoad() {

	var load_aes_lvl [32]byte

	// Open DB
	CommitLvlDbOpen()

	iter := commitsdb.NewIterator(nil, nil)

	var sum = sha256.New()

	var l commitLvlOngoingLoad

	for iok := iter.First(); iok; iok = iter.Next() {
		key := iter.Key()
		val := iter.Value()

		switch len(key) {
		case 8:
			if len(val) < 128 {
				log.Println("short block; val:", val)
				if !l.deletor {
					CommitLvlDbNewBatch()
					l.deletor = true
				}

			}
		case 16:
			if len(val) < 32 {
				log.Println("short commit; val", val)
				if !l.deletor {
					CommitLvlDbNewBatch()
					l.deletor = true
				}
			}
		default:
			continue
		}

		if l.deletor {
			switch len(key) {
			case 16, 8:
				_ = commitsdb.Delete(key, nil)
			}
			continue
		}

		switch len(key) {
		case 8:
			tag_height := uint64(binary.BigEndian.Uint64(key[:8]))
			l.commitLvlHandleBlock(tag_height, val, sum)
		case 16:
			address := fmt.Sprintf("%X", val)
			miner_mine_commit_pulled(address, new_utxotag_from_leveldb(key), UTAG_MINE)

			sum.Write(key)
			sum.Write(val)

			var aestmp [32]byte

			var aes, err5 = aes.NewCipher(val)
			if err5 != nil {
				log.Fatal("AES fault")
			}

			aes.Encrypt(aestmp[0:16], load_aes_lvl[0:16])
			aes.Decrypt(aestmp[16:32], load_aes_lvl[16:32])

			for i := 8; i < 16; i++ {
				aestmp[i], aestmp[8+i] = aestmp[8+i], aestmp[i]
			}

			aes.Encrypt(load_aes_lvl[0:16], aestmp[0:16])
			aes.Decrypt(load_aes_lvl[16:32], aestmp[16:32])

		}

	}

	const unused = 0
	l.commitLvlHandleBlock(unused, nil, sum)

	// Brake Pull
	iter.Release()
	err := iter.Error()
	if err != nil {
		log.Fatal("Commitdb Iter Error: ", err)
	}

	if l.deletor {
		CommitLvlDbWriteBatch()
	}

	// Close DB
	CommitLvlDbClose()

	// Fingerprint
	database_aes = load_aes_lvl
}