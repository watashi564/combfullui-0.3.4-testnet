# Combfullui v0.3.4

This repo has served its purpose. Development will proceed in another repo due to bad repo name.

Few changes to the upstream haircomb core wallet

---

## Changes


1. Make segmets_merkle_userinput 32 byte keyed - Enables anon-minization for merkle addresses
2. Make one error non-fatal. - This can actually occur when pulling over LAN
3. Public testnet. See testnetpaper.txt for details.
4. Attempted fix for the testnet-only double spending issue. Replaced sha256 with nethash and a code refactor.
5. Code cleanup, formatting and linting fix

## 2022-11-12

A security patch hash been applied. The version tag has been bumped to 0.3.5.1
