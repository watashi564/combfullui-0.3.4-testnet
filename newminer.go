package main

import (
	"crypto/sha256"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/syndtr/goleveldb/leveldb/util"
	"hash"
	"log"
	"net/http"
	"sort"
	"strings"
	"sync"
	"time"
)

type Parsed_Block struct {
	height int
	hash   string
	pbh    string
	txes   []*Parsed_TX
}

type Parsed_TX struct {
	commit string
	tag    utxotag
}

// JSON Structs
type Block_Info struct {
	//	Bits string `json:"bits"`
	//	Chainwork string `json:"chainwork"`
	//	Conf int `json:"confirmations"`
	//	Diff float64 `json:"difficulty"`
	Hash   string `json:"hash"`
	Height int    `json:"height"`
	//	MTime float64 `json:"mediantime"`
	//	MRoot string `json:"merkleroot"`
	//	NTX int `json:"nTx"`
	//	NBH string `json:"nextblockhash"`
	//	Nonce float64 `json:"nonce"`
	PBH string `json:"previousblockhash"`
	//	Size int `json:"size"`
	//	SSize int `json:"strippedsize"`
	//	Time float64 `json:"time"`
	TX []TX_Info `json:"tx"`
	//	Version int `json:"version"`
	//	VHex string `json:"versionHex"`
	//	Weight int `json:"weight"`
}

type TX_Info struct {
	VOut []map[string]interface{} `json:"vout"`
}

type WaitForBlock struct {
	Hash     string `json:"hash"`
	Height   int    `json:"height"`
	parsedOK bool
	duration int64
}

type Chaintip struct {
	Height int    `json:"height"`
	Status string `json:"status"`
}

var btc_is_caught_up_mutex sync.RWMutex
var btc_is_caught_up bool

var last_known_btc_height = -1
var last_known_btc_height_mutex sync.RWMutex

var mut sync.Mutex // locks both the count ints and the map
var results_map = make(map[int]*Parsed_Block)
var run = true
var run_mutex sync.RWMutex
var next_mine int
var next_download int
var max_block int
var dir int
var http_client *http.Client
var wg sync.WaitGroup

func make_client() *http.Client {
	client := &http.Client{
		Transport: &http.Transport{
			MaxIdleConnsPerHost: 10,
		},
		Timeout: 24 * time.Hour,
	}
	return client
}

func check_run() bool {
	run_mutex.RLock()
	defer run_mutex.RUnlock()
	return run
}

func stop_run() {
	run_mutex.Lock()
	defer run_mutex.Unlock()
	run = false
}

func set_last_known_btc_height(h int) {
	last_known_btc_height_mutex.Lock()
	defer last_known_btc_height_mutex.Unlock()
	last_known_btc_height = h
}

func check_last_known_btc_height() int {
	last_known_btc_height_mutex.RLock()
	defer last_known_btc_height_mutex.RUnlock()
	return last_known_btc_height
}

// ~~~GENERAL FUNC~~~
func (w *WaitForBlock) parse_wait_for_block(r json.RawMessage) (err error) {
	if w.duration != 0 {
		w.duration = time.Now().UnixNano() - w.duration
		if w.duration < 0 {
			w.duration = 0
		}
	}
	err = json.Unmarshal(r, w)
	if err == nil {
		if string(r) != "null" {
			w.parsedOK = true
		} else {
			err = errors.New("bad_json")
		}
	}

	return err
}

func handle_reorg_direct(our_height, inc_target_height int) {

	log.Println("reorg handle begun;", inc_target_height)

	// Setup
	height := uint32(our_height)
	target_height := uint32(inc_target_height)

	commit_cache_mutex.Lock()
	commits_mutex.Lock()

	commit_currently_loaded = utxotag{height: target_height, txnum: 0, outnum: 0, commitnum: 0}

	log.Println("reorg: height, target_height:", height, target_height)

	// delete blocks in the batch
	CommitLvlDbNewBatch()
	iter := commitsdb.NewIterator(&util.Range{Start: new_height_tag(uint64(target_height) + 1), Limit: new_height_tag(uint64(height + 1))}, nil)
	for iter.Next() {
		if len(iter.Key()) == 8 || len(iter.Key()) == 16 {
			commitsbatch.Delete(iter.Key())
		}
	}
	CommitLvlDbWriteBatch()
	iter.Release()
	err := iter.Error()
	if err != nil {
		log.Fatal("Hash Erase error: ", err)
	}

	for h_height := height; h_height > target_height; h_height-- {

		// Remove from live map
		pop_block(uint64(h_height))

	}

	type temp_commit struct {
		add [32]byte
		tag utxotag
	}
	var temp_commits_map = make(map[uint32][]temp_commit)

	// Pull commits to be removed, store them
	for add, tag := range commits {
		if tag.height > target_height {
			temp_commits_map[height] = append(temp_commits_map[height], temp_commit{add: add, tag: tag})
		}
	}

	log.Println("reorg: total blocks to remove commits:", len(temp_commits_map))

	// Remove live commits
	for height > target_height {

		// Sort the commits in the arrays
		sort.Slice(temp_commits_map[height], func(i, j int) (less bool) {
			return utag_cmp(
				&temp_commits_map[height][i].tag,
				&temp_commits_map[height][j].tag) > 0
		})

		unwritten := false
		for _, temp_commit := range temp_commits_map[height] {
			add := temp_commit.add
			// If combase has...
			if _, ok := combbases[add]; ok {
				// ...unmine
				segments_coinbase_unmine(add, uint64(height))
				// Delete
				delete(combbases, add)
			}

			// Commits delete
			delete(commits, add)

			// uncompute checksum
			CommitLvlDbUnCompute(add)

			// Used Keys?
			if enable_used_key_feature {
				used_key_commit_reorg(add, uint64(height))
			}

			// Merkle unmine
			merkle_unmine(add)

			// TX Leg Rollback
			tx_leg_rollback(add)

			// Unwritten
			unwritten = true

		}
		if unwritten && enable_used_key_feature {
			used_key_height_reorg(uint64(height))
		}

		height--
	}

	log.Println("finished removing commits")
	commit_rollback = nil
	commit_rollback_tags = nil
	resetgraph()

	temp_commits_map = nil
	commits_mutex.Unlock()
	commit_cache_mutex.Unlock()
}

func get_block_info_for_height(height int, client *http.Client) (*Block_Info, error) {

	var hash string
	log.Println("Getting info for height:", height)

	// Only return an error when loop starts and run is false
	if !check_run() {
		log.Println("get_block_info: run = false")
		return nil, errors.New("run is false")
	}

	// Get hash and remove \n
	var result_json json.RawMessage
	var call_err error

	switch dir {
	case 1:
		result_json, call_err = make_bitcoin_call(client, "getblockhash", fmt.Sprint(height))
		if call_err != nil {
			log.Println("get_block_info: block hash call error:", call_err)
			return nil, call_err
		}
		hash = string(result_json)
	case -1:
		hash = "\"" + our_block_at_height(uint64(height)).Hash + "\""
	}

	//log.Println("pulled block hash, attempting conv")

	//log.Println("hash conv success:", hash)

	// Get Block
	block_json, err := make_bitcoin_call(http_client, "getblock", hash+", "+"2")
	if err != nil {
		log.Println("get_block_info: block call error:", err)
		return nil, err
	}

	var block_info Block_Info
	err2 := json.Unmarshal(block_json, &block_info)
	if err2 != nil {
		log.Println(err2)
	}

	return &block_info, nil

}

func parse_block(block_info *Block_Info) *Parsed_Block {
	output := &Parsed_Block{}

	// Metadata
	output.height = block_info.Height
	output.hash = block_info.Hash
	output.pbh = block_info.PBH

	// Commits

	var j = 0

	for x, tx := range block_info.TX {
		// Check all outputs for P2WSH
		for i, vout := range tx.VOut {

			// If it has a scriptPubKey
			if vout["scriptPubKey"] != nil {
				scriptPubKey := vout["scriptPubKey"].(map[string]interface{})

				// If it has type
				if scriptPubKey["type"] != nil {
					my_type := scriptPubKey["type"].(string)

					// If type is "witness_v0_scripthash"
					if my_type == "witness_v0_scripthash" {

						// Pull the hex
						if scriptPubKey["hex"] != nil {

							hex := fmt.Sprintf("%v", scriptPubKey["hex"])

							output.txes = append(output.txes, &Parsed_TX{
								commit: strings.ToUpper(hex[4:]),
								tag:    new_utxotag(output.height, j, x, i),
							})

							j++
						}
					}
				}
			}
		}
	}

	return output
}

func miner(parsed_block *Parsed_Block) (*Commit_Block_Metadata, error) {

	// Compare inc_block hash to stored previous block hash
	otb := our_top_block()
	cond := no_blocks()

	if dir == -1 {
		otb = our_second_top_block()
		cond = one_or_no_blocks()
	}

	if !cond && otb.Hash != parsed_block.pbh {
		log.Println("hash mismatch while mining; ourtop.hash != inctop.prevhash:", otb.Hash, parsed_block.pbh)
		return nil, errors.New("hash mismatch")
	}

	CommitLvlDbNewBatch()

	// Format hash key
	hash_key := new_height_tag(uint64(parsed_block.height))

	if dir == -1 {
		commitsbatch.Delete(hash_key)
	}

	// Mine the commits
	for _, tx_data := range parsed_block.txes {
		miner_mine_commit_pulled(tx_data.commit, tx_data.tag, dir)
	}

	// Flush
	var commit_fingerprint hash.Hash
	if dir == -1 {
		commit_fingerprint = miner_mine_commit_pulled("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
			new_flush_utxotag(uint64(parsed_block.height)-1), 0)
	} else if dir == 1 {
		commit_fingerprint = miner_mine_commit_pulled("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
			new_flush_utxotag(uint64(parsed_block.height)), 0)
	}

	if commit_fingerprint == nil {
		commit_fingerprint = sha256.New()
	}

	commit_fingerprint.Write([]byte(parsed_block.hash))

	var sumbuf [32]byte
	var sum = sumbuf[0:0:32]

	sum = commit_fingerprint.Sum(sum)

	// Add the hash to the fingerprint
	fingerprint := fmt.Sprintf("%x", sum)

	// Build the block metadata
	metadata := &Commit_Block_Metadata{
		Height:      uint64(parsed_block.height),
		Hash:        parsed_block.hash,
		Fingerprint: fingerprint,
	}

	if dir == 1 {
		// Store the hash last, just in case.
		// Not just hash now, store hash CAT fingerprint
		log.Println("m", parsed_block.hash)
		commitsbatch.Put(hash_key, []byte(parsed_block.hash+fingerprint))
	}

	CommitLvlDbWriteBatch()

	log.Println("mined:", parsed_block.height)
	return metadata, nil
}

func downloader(height int) {
	my_client := make_client()

	defer wg.Done()

	for check_run() && height != 0 {

		// Download the block info
		block_info, err := get_block_info_for_height(height*dir, my_client)
		if err != nil || block_info == nil {
			log.Println("mine_blocks: dl err - get_block_info for height:", err.Error())
			stop_run()
			return
		}

		parsed_block := parse_block(block_info)

		if parsed_block.height != height*dir {
			log.Println("wrong height", parsed_block.height, height*dir)
			stop_run()
			return
		}

		mut.Lock()
		// If my height needs to be mined, mine it then break
		if parsed_block.height == next_mine {
			log.Println("PARSER (MINE ME!):", parsed_block.height, next_mine)

			// Else if the next mine height can be mined, mine it then check again
		} else if blk, ok := results_map[next_mine]; ok {
			log.Println("PARSER (MINE NEXT!):", parsed_block.height, next_mine)
			delete(results_map, next_mine)
			results_map[parsed_block.height] = parsed_block
			parsed_block = blk

			// Else if can't do shit, deposit payload then break
		} else {
			log.Println("PARSER (DROP OFF!):", parsed_block.height, next_mine)
			results_map[parsed_block.height] = parsed_block
			parsed_block = nil
		}
		next_download += dir
		if next_download*dir <= max_block*dir {
			height = next_download * dir
		} else {
			height = 0
		}

		mut.Unlock()

		for check_run() && parsed_block != nil {

			log.Println("APPLYING ANOTHER:", parsed_block.height)

			block_metadata, err := miner(parsed_block)
			if err != nil {
				stop_run()
				return
			}

			switch dir {
			case 1:
				put_block(uint64(parsed_block.height), block_metadata)
			case -1:
				pop_block(uint64(parsed_block.height))
			}

			mut.Lock()
			next_mine += dir
			parsed_block = results_map[next_mine]
			delete(results_map, next_mine)
			mut.Unlock()
		}
	}
}

func mine_blocks(start, finish, inc_dir int) {

	run = true
	results_map = make(map[int]*Parsed_Block)

	log.Println("mine_blocks: began:", start, finish, inc_dir)

	dir = inc_dir
	next_mine = start
	next_download = start + 6*dir
	max_block = finish

	if dir == 1 && next_download > max_block {
		next_download = max_block
	}
	if dir == -1 && next_download < max_block {
		next_download = max_block
	}

	// not making a copy makes the for loop below race with the goroutines that we spawn
	var next_download_copy = next_download

	log.Println("mine_blocks: vars set:", next_mine, next_download, max_block, dir)

	// While i, base start, is less than next download, go and increment
	log.Println(next_mine*dir, next_download*dir)

	// we must use a copy of next_download, because the original
	// could've been modified by the goroutines that we spawn
	for i := next_mine * dir; i <= next_download_copy*dir; i++ {
		wg.Add(1)
		go downloader(i)
		log.Println("mine_blocks: added downloader", i)
	}

	log.Println("mine_blocks: downloaders added")

	wg.Wait()

	if !check_run() {
		results_map = nil
		return
	}

	log.Println("final results:", len(results_map))

	log.Println("profile:", next_mine*dir, max_block*dir, start, finish)

	for i := next_mine * dir; i <= max_block*dir; i++ {

		var parsed_block = results_map[i*dir]

		log.Println("APPLYING FINAL:", parsed_block.height)
		block_metadata, err := miner(parsed_block)
		if err != nil {
			break
		}

		switch dir {
		case 1:
			put_block(uint64(parsed_block.height), block_metadata)
		case -1:
			pop_block(uint64(parsed_block.height))
		}

	}

	results_map = nil
}

// this needs to be synced with leveldb
var blocks_mutex sync.RWMutex
var blocks_map = make(map[uint64]*Commit_Block_Metadata)
var maximum_block uint64

func put_block(height uint64, blk *Commit_Block_Metadata) {
	blocks_mutex.Lock()
	defer blocks_mutex.Unlock()
	if maximum_block != 0 {
		if height != maximum_block+1 {
			log.Println("must put the next block height (height, max :", height, maximum_block)
			panic("must put the next block height")
		}
	}
	blocks_map[height] = blk
	maximum_block = height
}

func pop_block(height uint64) {
	blocks_mutex.Lock()
	defer blocks_mutex.Unlock()

	if height != maximum_block {
		log.Println("Must pop top block")
		panic("must pop top block")
	}

	maximum_block--
	if _, ok := blocks_map[height]; ok {
		delete(blocks_map, height)
	} else {
		log.Println("Must pop existing block")
		panic("must pop existing block")
	}
}

func our_block_at_height(height uint64) *Commit_Block_Metadata {
	blocks_mutex.RLock()
	defer blocks_mutex.RUnlock()

	if v, ok := blocks_map[height]; ok {
		return v
	}
	return &Commit_Block_Metadata{}
}
func one_or_no_blocks() bool {
	blocks_mutex.RLock()
	defer blocks_mutex.RUnlock()

	return len(blocks_map) <= 1
}
func no_blocks() bool {
	blocks_mutex.RLock()
	defer blocks_mutex.RUnlock()

	return len(blocks_map) == 0
}
func our_top_block() *Commit_Block_Metadata {
	blocks_mutex.RLock()
	defer blocks_mutex.RUnlock()

	if v, ok := blocks_map[maximum_block]; ok {
		return v
	}
	return &Commit_Block_Metadata{}
}
func our_second_top_block() *Commit_Block_Metadata {
	blocks_mutex.RLock()
	defer blocks_mutex.RUnlock()

	if v, ok := blocks_map[maximum_block-1]; ok {
		return v
	}
	return &Commit_Block_Metadata{}
}
func chain_size() (ret uint64) {
	blocks_mutex.RLock()
	defer blocks_mutex.RUnlock()

	return maximum_block
}

func find_reorg2(client *http.Client, hc_height uint64, u_config UserConfig) (uint64, error) {

	log.Println("find_reorg: started")

	// Set a stop limit
	var lowest uint64 = 481823
	if u_config.regtest {
		lowest = 0
	}

	for hc_height > lowest {

		var ours = our_block_at_height(hc_height).Hash

		result_json, err := make_bitcoin_call(client, "getblockhash", fmt.Sprint(hc_height))
		if err != nil {
			return 0, err
		}
		result := strings.Trim(string(result_json), `"`)

		if result == ours {
			return hc_height, nil
		}

		hc_height--
	}
	return lowest, nil
}

func new_miner_start() {

	// make the http client for main calls
	http_client = make_client()

	var oldWaitForBlock WaitForBlock

	log.Println("MINER START")
outer:
	for {

		if oldWaitForBlock.parsedOK && oldWaitForBlock.Height < 481824 {

			time.Sleep(10 * time.Second)

		} else {
			time.Sleep(1 * time.Second)
		}

		var wfb WaitForBlock

		// Get waitfornewblock data and parse
		data, err := make_bitcoin_call(http_client, "waitfornewblock", "1000")
		if err != nil {
			btc_is_caught_up_mutex.Lock()
			btc_is_caught_up = false
			btc_is_caught_up_mutex.Unlock()
			oldWaitForBlock = WaitForBlock{}
			set_connected(false)
			continue outer
		}

		err = wfb.parse_wait_for_block(data)
		if err != nil {
			btc_is_caught_up_mutex.Lock()
			btc_is_caught_up = false
			btc_is_caught_up_mutex.Unlock()
			oldWaitForBlock = WaitForBlock{}
			set_connected(false)

			continue outer
		}

		// If waitfornewblock returns a new block
		if wfb.Hash != oldWaitForBlock.Hash {

			var chainTips []Chaintip
			var chainTipData json.RawMessage
			var err error

			set_last_known_btc_height(wfb.Height)

			// Make a call to get BTC chain tips, if appropriate heights. Else throw errors and restart process
			if oldWaitForBlock.Height == 0 || oldWaitForBlock.Height+1 == wfb.Height {

				chainTipData, err = make_bitcoin_call(http_client, "getchaintips", "")

			} else {
				err = errors.New("getchaintips not in use")
			}

			oldWaitForBlock = wfb

			if err == nil {
				err = json.Unmarshal(chainTipData, &chainTips)

			}

			if err == nil {
				var activeHeight, headersOnlyHeight int
				for _, tip := range chainTips {
					switch tip.Status {
					case "active":
						activeHeight = tip.Height
						if headersOnlyHeight < tip.Height {
							headersOnlyHeight = tip.Height
						}
					case "headers-only", "valid-headers":
						if headersOnlyHeight < tip.Height {
							headersOnlyHeight = tip.Height
						}
					}
				}
				if !(activeHeight == 0 || headersOnlyHeight == 0) {

					log.Println("tips:", activeHeight, headersOnlyHeight)
					btc_is_caught_up_mutex.Lock()
					btc_is_caught_up = activeHeight == headersOnlyHeight
					btc_is_caught_up_mutex.Unlock()
				}
			}

		}

		hash := wfb.Hash
		height := uint64(wfb.Height)
		set_connected(true)

		// We are synced
		if our_top_block().Hash == hash {
			continue outer
		}

		// Get Haircomb's highest known block
		comb_height := uint64(chain_size())

		// If not regtest, make the current height the first COMB block
		if !u_config.regtest && comb_height < 481823 {
			comb_height = 481823
		}

		var ourhash_at_btc_height = our_block_at_height(height).Hash

		log.Println(hash, "==", ourhash_at_btc_height)

		if ourhash_at_btc_height == hash {
			log.Println("case 1")
			btc_is_caught_up_mutex.RLock()
			read_btc_caughtup := btc_is_caught_up
			btc_is_caught_up_mutex.RUnlock()
			if read_btc_caughtup {
				switch u_config.reorg_type {
				case "miner":
					log.Println("reorg type \"miner\"")
					mine_blocks(int(comb_height), int(height)+1, -1)
				case "direct":
					log.Println("reorg type \"direct\"")
					handle_reorg_direct(int(comb_height), int(height))
				}
			}
			//mine_blocks(int(curr_height), int(height)+1, -1)
			continue outer
		}

		log.Println(height, "<=", comb_height)

		if height <= comb_height {
			var target_height uint64

			target_height, err = find_reorg2(http_client, height, u_config)
			if err != nil {
				log.Println("mine_loop: find reorg err:", err)
				continue outer
			}
			log.Println("case 2")
			switch u_config.reorg_type {
			case "miner":
				log.Println("reorg type \"miner\"")
				mine_blocks(int(comb_height), int(target_height)+1, -1)
			case "direct":
				log.Println("reorg type \"direct\"")
				handle_reorg_direct(int(comb_height), int(target_height))
			}

			continue outer
		}

		result_json, err := make_bitcoin_call(http_client, "getblockhash", fmt.Sprint(comb_height))
		if err != nil {
			log.Println("get_block_info: block hash call error")
			continue outer
		}

		var btc_hash_at_our_height = strings.Trim(string(result_json), `"`)

		var our_highest_block = our_top_block()

		log.Println("?", btc_hash_at_our_height, our_highest_block.Hash)

		if btc_hash_at_our_height == our_highest_block.Hash || no_blocks() {

		} else {
			var target_height uint64

			target_height, err = find_reorg2(http_client, comb_height, u_config)
			if err != nil {
				log.Println("mine_loop: find reorg err:", err)
				continue outer
			}
			log.Println("case 3")
			switch u_config.reorg_type {
			case "miner":
				log.Println("reorg type \"miner\"")
				mine_blocks(int(comb_height), int(target_height)+1, -1)
			case "direct":
				log.Println("reorg type \"direct\"")
				handle_reorg_direct(int(comb_height), int(target_height))
			}
			continue outer
		}
		log.Println("case 4")
		// finally fast forward
		mine_blocks(int(comb_height)+1, int(height), 1)
		continue outer
	}

}